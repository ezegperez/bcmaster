module.exports = {
  root: true,
  extends: '@react-native-community',
  plugins: ['import'],
  settings: {
    'import/resolver': {
      node: {
        paths: ['src'],
        alias: {
          _src: "./src",
          _api: "./src/api",
          _fonts: './src/assets/fonts',
          _images: './src/assets/images',
          _components: './src/components',
          _features: './src/features',
          _navigations: './src/navigations',
          _services: "./src/services",
          _styles: './src/styles',
          _utils: './src/utils',
        },
      },
    },
  },
};