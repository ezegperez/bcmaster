import React from 'react';
import Navigator from '_navigations';
import { Provider } from 'react-redux';
import store from './store'

const App = () => (
    <Provider store={store}>
        <Navigator />

    </Provider>
);

export default App;