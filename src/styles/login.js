import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    scroll: {
        backgroundColor: 'transparent',
    },
    
    container: {
        margin: 8,
        marginTop: Platform.select({ ios: 8, android: 32 }),
        flex: 1,
    },
    
    contentContainer: {
        padding: 8,
    },
    
    buttonContainer: {
        paddingTop: 8,
        margin: 8,
    },
    
    safeContainer: {
        flex: 1,
        backgroundColor: '#E8EAF6',
    },
});