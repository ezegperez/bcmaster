import { AsyncStorage } from 'react-native';
import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore, autoRehydrate } from 'redux-persist';
import createFilter from 'redux-persist-transform-filter';
import promise from 'redux-promise-middleware';
import { reducer as dataReducer } from '_api/reducer';
import { reducer as servicesReducer } from './services/reducer';
import * as persistActionCreators from './services/persist/actions';

const appReducer = combineReducers({
	services: servicesReducer,
	//data: dataReducer,
});


const devtools = composeWithDevTools({ realtime: true })
const enhancer = devtools(
	applyMiddleware(
		promise,
		thunk,
	),
	autoRehydrate()
);

const store = createStore(
	appReducer,
	enhancer,
	//autoRehydrate(),
);

const saveAndLoadSessionFilter = createFilter(
	'services',
	['session'],
	['session']
);

export const persist = persistStore(store, {
	storage: AsyncStorage,
	blacklist: ['data'],
	transforms: [saveAndLoadSessionFilter],
}, () => store.dispatch(persistActionCreators.update({ isHydrated: true })));

export default store;