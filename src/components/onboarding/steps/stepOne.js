import React, { useState } from 'react';
import { View, SafeAreaView, ScrollView, Text, ImageComponent } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import { RaisedTextButton } from 'react-native-material-buttons';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { TextInput } from 'react-native-gesture-handler';

export default ({ renderPasswordAccessory, styles, errors = {}, onNext }) => {

    let [name, setName] = useState('');
    let [email, setEmail] = useState('');
    let [password, setPassword] = useState('');
    let [isSecureTextEnabled, setIsSecureTextEnabled] = useState(true)

    let secondInput, thirdInput;

    function onSubmit() {

    }
    return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
            <Text>
                How do you want to contact you?
            </Text>
            <Text>
                What is your way of communication
            </Text>
            <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#fff',
                height: 10,
                maxHeight: 40,
                marginTop: 20,
                marginBottom: 20
            }} >
                <Icon style={{ padding: 10, }} name="telegram-plane" size={20} color="#000" />
                <TextInput
                    style={{
                        flex: 1,
                        paddingTop: 10,
                        paddingRight: 10,
                        paddingBottom: 10,
                        paddingLeft: 0,
                        backgroundColor: '#fff',

                        color: '#424242',
                    }}
                    placeholder="Telegram"
                    onChangeText={(searchString) => { this.setState({ searchString }) }}
                    underlineColorAndroid="transparent"
                />

            </View>
            <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#fff',
                height: 10,
                maxHeight: 40,

                marginBottom: 20
            }} >
                <Icon style={{ padding: 10, }} name="telegram-plane" size={20} color="#000" />
                <TextInput
                    style={{
                        flex: 1,
                        paddingTop: 10,
                        paddingRight: 10,
                        paddingBottom: 10,
                        paddingLeft: 0,
                        backgroundColor: '#fff',

                        color: '#424242',
                    }}
                    placeholder="Telegram"
                    onChangeText={(searchString) => { this.setState({ searchString }) }}
                    underlineColorAndroid="transparent"
                />

            </View>
            <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#fff',
                height: 10,
                maxHeight: 40,

                marginBottom: 20
            }} >
                <Icon style={{ padding: 10, }} name="telegram-plane" size={20} color="#000" />
                <TextInput
                    style={{
                        flex: 1,
                        paddingTop: 10,
                        paddingRight: 10,
                        paddingBottom: 10,
                        paddingLeft: 0,
                        backgroundColor: '#fff',

                        color: '#424242',
                    }}
                    placeholder="Telegram"
                    onChangeText={(searchString) => { this.setState({ searchString }) }}
                    underlineColorAndroid="transparent"
                />

            </View>


            <RaisedTextButton
                onPress={onNext}
                title='next'
                color={TextField.defaultProps.tintColor}
                titleColor='white'
            />

        </View >
    )
}