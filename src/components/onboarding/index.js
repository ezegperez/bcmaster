import React, { Component, useState, useRef } from 'react';
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';
import { View, SafeAreaView, ScrollView } from 'react-native';

import Swiper from 'react-native-swiper';

import { TextField } from 'react-native-material-textfield';
import { RaisedTextButton } from 'react-native-material-buttons';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { MaterialIndicator } from 'react-native-indicators';

import * as session from '_services/session';
import * as api from '_services/api';

import Step1 from '_components/onboarding/steps/stepOne'
import styles from '_styles/login';





const OnBoardingScreen = (props) => {

    let [email, setEmail] = useState('');
    let [password, setPassword] = useState('');
    let [errors, setErrors] = useState({});
    let [isSecureTextEnabled, setIsSecureTextEnabled] = useState(true);
    let [isLoading, setIsLoading] = useState(false);
    let swiperEl = useRef()



    function updateEmail(name, ref) {
        console.log("name " + name);
        console.log("ref " + ref);
        setEmail(ref);
    }

    function updatePass(name, ref) {
        console.log("name " + name);
        console.log("ref " + ref);
        setPassword(ref);
    }

    function onFocus() {
        setErrors({})
    }
    function onChangeEmail(email) {
        setEmail(email)
    }
    function onChangePassword(password) {
        setPassword(password)
    }

    const onSubmit = () => {
        setIsLoading(true)
        setErrors({});
        dismissKeyboard();

        session.authenticate(email, password)
            .then(() => {
                //this.setState(this.initialState);
                setIsLoading(false)
                console.log('props ', props)
                //const routeStack = 
                props.navigation.navigate("Home")
                //props.navigator.jumpTo(routeStack[3]);
            })
            .catch((exception) => {
                // Displays only the first error message
                const error = api.exceptionExtractError(exception);
                setIsLoading(false);
                setErrors(error ? { error } : {})

                if (!error) {
                    throw exception;
                }
            });
    }


    const next = () => {
        console.log(swiperEl)
        swiperEl.current.scrollBy(1);
    }

    return (
        <SafeAreaView style={styles.safeContainer}>
            <Swiper ref={swiperEl}>
                <Step1 styles={styles} onNext={next} />
                <Step1 styles={styles} onNext={next} />
                <Step1 styles={styles} onNext={next} />

            </Swiper>

        </SafeAreaView>
    );
}

export default OnBoardingScreen;