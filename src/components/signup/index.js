import React, { useState } from 'react';
import { View, SafeAreaView, ScrollView, Text } from 'react-native';
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';
import { TextField } from 'react-native-material-textfield';
import { RaisedTextButton } from 'react-native-material-buttons';
import * as session from '_services/session';
import * as api from '_services/api';

import styles from '_styles/login';

const SignUpScreen = ({ navigation, errors = {} }) => {

    let [name, setName] = useState('');
    let [email, setEmail] = useState('');
    let [password, setPassword] = useState('');
    let [isSecureTextEnabled, setIsSecureTextEnabled] = useState(true)

    let secondInput, thirdInput;

    function onSubmit() {
        //setIsLoading(true)
        //setErrors({});
        dismissKeyboard();


        session.signup(name, email, password)
            .then(() => {
                //this.setState(this.initialState);
                //setIsLoading(false)
                console.log('props ')
                //const routeStack = 
                navigation.navigate('App', { screen: 'OnBoarding' })

                //props.navigator.jumpTo(routeStack[3]);
            })
            .catch((exception) => {
                // Displays only the first error message
                console.log('exception', exception)
                const error = api.exceptionExtractError(exception);
                //setIsLoading(false);
                console.log('error', error)
                //setErrors(error ? { error } : {})
                // this.setState({
                //   isLoading: false,
                //   ...(error ? { error } : {}),
                // });

                if (!error) {
                    throw exception;
                }
            });
    }
    return (
        <SafeAreaView style={styles.container}>
            <Text>
                Hello! Get started with your account
            </Text>
            <Text>
                One account for all your digital presence
            </Text>
            <TextField
                label="What's your name?"
                keyboardType='default'
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                onChangeText={(name) => setName(name)}
                returnKeyType='next'
                error={errors.name}
                onSubmitEditing={() => secondInput.focus()}
            />
            <TextField
                label="Your email address"
                keyboardType='email-address'
                autoCapitalize='none'
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                onChangeText={(email) => setEmail(email)}
                returnKeyType='next'
                error={errors.email}
                ref={ref => {
                    secondInput = ref;
                }}
                onSubmitEditing={() => thirdInput.focus()}

            />

            <TextField
                secureTextEntry={isSecureTextEnabled}
                autoCapitalize='none'
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                clearTextOnFocus={true}
                onChangeText={(pass) => setPassword(pass)}
                returnKeyType='done'
                label='Create a password'
                error={errors.password}
                maxLength={30}
                characterRestriction={20}
                ref={ref => {
                    thirdInput = ref;
                }}
            //renderRightAccessory={renderPasswordAccessory}
            />
            <RaisedTextButton
                onPress={onSubmit}
                title='submit'
                color={TextField.defaultProps.tintColor}
                titleColor='white'
            />

        </SafeAreaView >
    )
}

export default SignUpScreen;