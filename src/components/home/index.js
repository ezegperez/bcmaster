import React, { useEffect, use } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { SafeAreaView, View, Text } from 'react-native';
import * as UserActions from '_services/user/actions'


const HomeScreen = () => {
  let profile = useSelector(state => state.services.user.profile)
  let dispatch = useDispatch();


  useEffect(() => {
    if (!profile) {
      dispatch(UserActions.fetchUserProfile('Jnichol'));
    }
    console.log('profile', profile)
  }, [profile, dispatch]);



  return (
    <SafeAreaView>
      <Text>Hello {profile.name} </Text>
    </SafeAreaView>
  )
};

export default HomeScreen;