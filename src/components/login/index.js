import React, { useState } from 'react';
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';
import { View, SafeAreaView, ScrollView } from 'react-native';

import { TextField } from 'react-native-material-textfield';
import { RaisedTextButton } from 'react-native-material-buttons';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { MaterialIndicator } from 'react-native-indicators';

import * as session from '_services/session';
import * as api from '_services/api';

import styles from '_styles/login';

const LoginScreen = (props) => {

  let [email, setEmail] = useState('');
  let [password, setPassword] = useState('');
  let [errors, setErrors] = useState({});
  let [isSecureTextEnabled, setIsSecureTextEnabled] = useState(true);
  let [isLoading, setIsLoading] = useState(false);


  function updateEmail(name, ref) {
    console.log("name " + name);
    console.log("ref " + ref);
    setEmail(ref);
  }

  function updatePass(name, ref) {
    console.log("name " + name);
    console.log("ref " + ref);
    setPassword(ref);
  }

  function onFocus() {
    //let { errors = {} } = this.state;
    // for (let name in errors) {
    //   let ref = this[name];
    //   if (ref && ref.isFocused()) {
    //     delete errors[name];
    //   }
    // }
    // this.setState({ errors });
    setErrors({})
  }
  function onChangeEmail(email) {
    setEmail(email)
  }
  function onChangePassword(password) {
    setPassword(password)
  }
  // function onChangeText(text) {
  //   ['email', 'pass']
  //     .map((name) => ({ name, ref: this[name] }))
  //     .forEach(({ name, ref }) => {
  //       if (ref.isFocused()) {
  //         this.setState({ [name]: text });
  //       }
  //     });
  // }

  function renderPasswordAccessory() {

    let name = isSecureTextEnabled ?
      'visibility' :
      'visibility-off';

    return (
      <MaterialIcon
        size={24}
        name={name}
        color={TextField.defaultProps.baseColor}
        onPress={this.onAccessoryPress}
        suppressHighlighting={true}
      />
    );
  }

  const onSubmit = () => {
    setIsLoading(true)
    setErrors({});
    dismissKeyboard();
    // this.setState({
    //   isLoading: true,
    //   error: '',
    // });

    session.authenticate(email, password)
      .then(() => {
        //this.setState(this.initialState);
        setIsLoading(false)
        console.log('props ', props)
        //const routeStack = 
        props.navigation.navigate("Home")
        //props.navigator.jumpTo(routeStack[3]);
      })
      .catch((exception) => {
        // Displays only the first error message
        const error = api.exceptionExtractError(exception);
        setIsLoading(false);
        setErrors(error ? { error } : {})
        // this.setState({
        //   isLoading: false,
        //   ...(error ? { error } : {}),
        // });

        if (!error) {
          throw exception;
        }
      });
  }

  return (
    <SafeAreaView style={styles.safeContainer}>
      <ScrollView
        style={styles.scroll}
        contentContainerStyle={styles.contentContainer}
        keyboardShouldPersistTaps='handled'
      >
        <View style={styles.container}>

          <TextField
            label='Email Address'
            ref={updateEmail}
            keyboardType='email-address'
            autoCapitalize='none'
            autoCorrect={false}
            enablesReturnKeyAutomatically={true}
            onFocus={onFocus}
            onChangeText={onChangeEmail}
            returnKeyType='next'
            error={errors.email}
          />

          <TextField
            ref={updatePass}
            secureTextEntry={isSecureTextEnabled}
            autoCapitalize='none'
            autoCorrect={false}
            enablesReturnKeyAutomatically={true}
            clearTextOnFocus={true}
            onFocus={onFocus}
            onChangeText={onChangePassword}
            returnKeyType='done'
            label='Password'
            error={errors.password}
            title='Choose wisely'
            maxLength={30}
            characterRestriction={20}
            renderRightAccessory={renderPasswordAccessory}
          />

        </View>

        {isLoading ? (
          <View style={styles.flex}>
            <MaterialIndicator />
          </View>
        ) : (
            <View style={styles.buttonContainer}>
              <RaisedTextButton
                onPress={onSubmit}
                title='submit'
                color={TextField.defaultProps.tintColor}
                titleColor='white'
              />
            </View>
          )
        }

      </ScrollView>
    </SafeAreaView>
  );

  // constructor(props) {
  //   super(props);
  //   this.onFocus = this.onFocus.bind(this);
  //   this.onChangeText = this.onChangeText.bind(this);

  //   this.emailRef = this.updateRef.bind(this, 'email');
  //   this.passRef = this.updateRef.bind(this, 'pass');
  //   this.renderPasswordAccessory = this.renderPasswordAccessory.bind(this);

  //   this.state = {
  //     secureTextEntry: true,
  //     isLoading: false
  //   };
  // }







  // onChangeText(text) {
  //   ['email', 'pass']
  //     .map((name) => ({ name, ref: this[name] }))
  //     .forEach(({ name, ref }) => {
  //       if (ref.isFocused()) {
  //         this.setState({ [name]: text });
  //       }
  //     });
  // }


  // renderPasswordAccessory() {
  //   let { secureTextEntry } = this.state;

  //   let name = secureTextEntry ?
  //     'visibility' :
  //     'visibility-off';

  //   return (
  //     <MaterialIcon
  //       size={24}
  //       name={name}
  //       color={TextField.defaultProps.baseColor}
  //       onPress={this.onAccessoryPress}
  //       suppressHighlighting={true}
  //     />
  //   );
  // }



  // let { errors = {}, secureTextEntry } = this.state;


}

export default LoginScreen;