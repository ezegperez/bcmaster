import React, { Component } from 'react';
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';
import { View, SafeAreaView, ScrollView } from 'react-native';

import { TextField } from 'react-native-material-textfield';
import { RaisedTextButton } from 'react-native-material-buttons';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { MaterialIndicator } from 'react-native-indicators';

import * as session from '_services/session';
import * as api from '_services/api';

import styles from '_styles/login';

export default class LoginScreen extends Component {

  constructor(props) {
    super(props);
    this.onFocus = this.onFocus.bind(this);
    this.onChangeText = this.onChangeText.bind(this);

    this.emailRef = this.updateRef.bind(this, 'email');
    this.passRef = this.updateRef.bind(this, 'pass');
    this.renderPasswordAccessory = this.renderPasswordAccessory.bind(this);

    this.state = {
      secureTextEntry: true,
      isLoading: false
    };
  }

  updateRef(name, ref) {
    console.log("name " + name);
    console.log("ref " + ref);
    this[name] = ref;
  }

  onFocus() {
    let { errors = {} } = this.state;
    for (let name in errors) {
      let ref = this[name];
      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }
    this.setState({ errors });
  }

  onSubmit = () => {
    this.setState({
      isLoading: true,
      error: '',
    });
    dismissKeyboard();

    session.authenticate(this.state.email, this.state.pass).then(() => {
      this.setState(this.initialState);
      const routeStack = this.props.navigator.getCurrentRoutes();
      this.props.navigator.jumpTo(routeStack[3]);
    }).catch((exception) => {
      // Displays only the first error message
      const error = api.exceptionExtractError(exception);
      this.setState({
        isLoading: false,
        ...(error ? { error } : {}),
      });

      if (!error) {
        throw exception;
      }
    });
  }

  onChangeText(text) {
    ['email', 'pass']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          this.setState({ [name]: text });
        }
      });
  }


  renderPasswordAccessory() {
    let { secureTextEntry } = this.state;

    let name = secureTextEntry ?
      'visibility' :
      'visibility-off';

    return (
      <MaterialIcon
        size={24}
        name={name}
        color={TextField.defaultProps.baseColor}
        onPress={this.onAccessoryPress}
        suppressHighlighting={true}
      />
    );
  }

  render() {

    let { errors = {}, secureTextEntry } = this.state;

    return (
      <SafeAreaView style={styles.safeContainer}>
        <ScrollView
          style={styles.scroll}
          contentContainerStyle={styles.contentContainer}
          keyboardShouldPersistTaps='handled'
        >
          <View style={styles.container}>

            <TextField
              label='Email Address'
              ref={this.emailRef}
              keyboardType='email-address'
              autoCapitalize='none'
              autoCorrect={false}
              enablesReturnKeyAutomatically={true}
              onFocus={this.onFocus}
              onChangeText={this.onChangeText}
              returnKeyType='next'
              error={errors.email}
            />

            <TextField
              ref={this.passRef}
              secureTextEntry={secureTextEntry}
              autoCapitalize='none'
              autoCorrect={false}
              enablesReturnKeyAutomatically={true}
              clearTextOnFocus={true}
              onFocus={this.onFocus}
              onChangeText={this.onChangeText}
              returnKeyType='done'
              label='Password'
              error={errors.password}
              title='Choose wisely'
              maxLength={30}
              characterRestriction={20}
              renderRightAccessory={this.renderPasswordAccessory}
            />

          </View>

          {this.state.isLoading ? (
            <View style={styles.flex}>
              <MaterialIndicator />
            </View>
          ) : (
              <View style={styles.buttonContainer}>
                <RaisedTextButton
                  onPress={this.onSubmit}
                  title='submit'
                  color={TextField.defaultProps.tintColor}
                  titleColor='white'
                />
              </View>
            )
          }

        </ScrollView>
      </SafeAreaView>
    )
  }
}