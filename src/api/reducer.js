
import { combineReducers } from 'redux';
//import { User } from '_api/users/reducer';

export const reducers = combineReducers({
    //  users: User,
});

const rootReducer = (state, action) => {
    return reducers(state, action);
}

export default rootReducer;