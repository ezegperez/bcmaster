/* global fetch */

import fetchival from 'fetchival';
import apiConfig from './config';

export const exceptionExtractError = (exception) => {
	if (!exception.Errors) return false;
	let error = false;
	const errorKeys = Object.keys(exception.Errors);
	if (errorKeys.length > 0) {
		error = exception.Errors[errorKeys[0]][0].message;
	}
	return error;
};

export const fetchPublic = (endpoint, headers = {}) => {
	debugger;
	return fetchival(`${apiConfig.url}${endpoint}`, { headers })
}
export const fetchPrivate = (endpoint, headers = {}) => {
	return fetchival(`${apiConfig.url}${endpoint}`)
}

export const fetchApi = (endPoint, payload = {}, method = 'get') => {
	const accessToken = null;//---Por ahora null

	console.log(`${apiConfig.url}${endPoint}`, payload)
	return fetchival(`${apiConfig.url}${endPoint}`, {
		// headers: _.pickBy({
		// 	...(accessToken ? {
		// 		Authorization: `Bearer ${accessToken}`,
		// 	} : {
		// 		'Client-ID': apiConfig.clientId,
		// 	}),
		// 	...headers,
		// }, item => !_.isEmpty(item)),
		headers: {
			...(accessToken ? {
				Authorization: `Bearer ${accessToken}`,
			} : {
					'Client-ID': apiConfig.clientId,
				}),
			...headers,
		},
	})[method.toLowerCase()](payload)
		.catch((e) => {
			console.log(e, e.response)
			if (e.response && e.response.json) {
				e.response.json().then((json) => {
					if (json) throw json;
					throw e;
				});
			} else {
				throw e;
			}
		});
}