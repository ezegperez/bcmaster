import * as actionTypes from './actionTypes';
import * as fetches from './api'

export const fetchUserProfile = user => ({
    type: actionTypes.GET_USER_PROFILE,
    payload: fetches.getUserProfile(user),
});