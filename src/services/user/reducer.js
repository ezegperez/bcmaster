import * as ActionTypes from "./actionTypes";

export const initialState = {
    profile: 0,
    username: '',
    email: '',
};

export const userReducer = (state = initialState, action) => {
    console.log('UserReducer', action)
    switch (action.type) {
        case ActionTypes.GET_USER_PROFILE_FULFILLED: {
            return {
                ...state,
                profile: action.payload
            }
        }

        default: {
            return state;
        }
    }
};

