import store from '_src/store';

export const get = () => store.getState().services.session;