import { Buffer } from 'buffer';
import { fetchApi } from '_services/api';
import apiConfig from '_services/api/config';

const endPoints = {
	authenticate: '/auth/login',
	revoke: '/auth/revoke',
	refresh: '/auth/refresh',
	signup: '/auth/sign-up'
};

export const authenticate = (email, password) => fetchApi(endPoints.authenticate, { username: email, password }, 'post', {
	Authorization: `Basic ${new Buffer(`${email}:${password}`).toString('base64')}`,
});

export const refresh = (token, user) => fetchApi(endPoints.refresh, { token, user }, 'post', {
	'Client-ID': apiConfig.clientId,
	Authorization: null,
});

export const revoke = tokens => fetchApi(endPoints.revoke, { tokens }, 'post');

export const signup = (name, email, password) => fetchApi(endPoints.signup, { name, email, password, username: email }, 'post')