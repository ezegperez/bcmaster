import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import HomeScreen from '_components/home';
import OnBoardingScreen from '_components/onboarding';
//import AboutScreen from '_features/about';

const TabNavigatorConfig = {
  initialRouteName: 'Home',
  header: null,
  headerMode: 'none',
};

const RouteConfigs = {
  Home: {
    screen: HomeScreen,
  },
  // About:{
  //   screen:AboutScreen,
  // },
};

const Drawer = createDrawerNavigator();
const AppNavigator = () => {
  return (
    <Drawer.Navigator initialRouteName="Home">
      <Drawer.Screen name="Home" component={HomeScreen} />
      <Drawer.Screen name="OnBoarding" component={OnBoardingScreen} />
    </Drawer.Navigator>
  )

}
//const AppNavigator = createBottomTabNavigator(RouteConfigs, TabNavigatorConfig);

export default AppNavigator;