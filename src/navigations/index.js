import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'
import AuthNavigator from './auth-navigator';
import AppNavigator from './app-navigator';

// const RootNavigator = createSwitchNavigator(
//   {
//     Auth: AuthNavigator,
//     App: AppNavigator,
//   },
//   {
//     initialRouteName: 'Auth',
//   },
// );
const isLoggedIn = false;
const Stack = createStackNavigator()
//export default createAppContainer(RootNavigator);

export default () => {


  return (
    <NavigationContainer >
      <Stack.Navigator initialRouteName="App" headerMode='none'>
        {/* {isLoggedIn ? (
          <Stack.Screen name="App" component={AppNavigator} />
        ) : (
            <Stack.Screen name="Auth" component={AuthNavigator} />
          )} */}
        <Stack.Screen name="App" component={AppNavigator} />
        <Stack.Screen name="Auth" component={AuthNavigator} />
      </Stack.Navigator>
    </NavigationContainer>
  )
};