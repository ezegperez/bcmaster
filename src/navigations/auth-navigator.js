import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import LoginScreen from '_components/login';
import SignUpScreen from '_components/signup'

const AuthNavigatorConfig = {
  initialRouteName: 'SingUp',
  header: null,
  headerMode: 'none',
};

const RouteConfigs = {
  Login: LoginScreen,
  SingUp: SignUpScreen,
};
const Stack = createStackNavigator();

const AuthNavigator = () => {
  return (
    <Stack.Navigator initialRouteName="SingUp" headerMode='none'>
      <Stack.Screen name="Login" component={LoginScreen} headerMode='none' />
      <Stack.Screen name="SingUp" component={SignUpScreen} />
    </Stack.Navigator>
  )

}

export default AuthNavigator;